import React from "react";
import Botonera from "./components/botonera";
import Caja from "./components/caja";

function App() {
  return (
    <div className="App">
      <Botonera />
      <Caja />
    </div>
  );
}

export default App;
