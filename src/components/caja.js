import React, { Component } from "react";

import ColorAppStore from "../sotores/ColorAppStore";

class Caja extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: ColorAppStore.getActiveColor(),
    };
  }

  componentDidMount() {
    ColorAppStore.on("storeUpdated", this.updateColor);
  }

  componentWillUnmount() {
    ColorAppStore.removeListener("storeUpdated", this.updateColor);
  }

  updateColor = () => {
    this.setState({ color: ColorAppStore.getActiveColor() });
  };

  render() {
    return (
      <div className="Caja">
        <div
          className="color-container"
          style={{ background: this.state.color }}
        ></div>
      </div>
    );
  }
}

export default Caja;
