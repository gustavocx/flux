import React from "react";
import * as ColorAppActions from "../actions/ColorAppActions";

const Botonera = () => {
  const buttonClick = (color) => {
    ColorAppActions.changeColor(color);
  };

  return (
    <div className="Botonera">
      <button
        type="button"
        class="btn btn-success m-3"
        onClick={() => buttonClick("green")}
      >
        Verde
      </button>
      <button
        type="button"
        class="btn btn-warning m-3"
        onClick={() => buttonClick("yellow")}
      >
        Amarillo
      </button>
      <button
        type="button"
        class="btn btn-danger m-3"
        onClick={() => buttonClick("red")}
      >
        Rojo
      </button>
    </div>
  );
};

export default Botonera;
